# C3D2 NixOS user module

NixOS module intended to be used by people regularly visiting C3D2 and wanting to stream audio, have a filled known_host's file, etc.

## Usage

Add or merge the following settings to your `flake.nix`:

```nix
{
  inputs = {
    c3d2-user-module.url = "git+https://gitea.c3d2.de/C3D2/nix-user-module.git";
  };

  outputs = { c3d2-user-module }: {
    nixosConfigurations.HOSTNAME = {
      modules = [
        c3d2-user-module.nixosModule
      ];
    };
  };
}
```

If you are using [nixos-modules](https://github.com/NuschtOS/nixos-modules) you need to add a follows to the c3d2-user-module:

```nix
{
  inputs = {
    c3d2-user-module = {
      url = "git+https://gitea.c3d2.de/C3D2/nix-user-module.git";
      inputs.nixos-modules.follows = "nixos-modules";
    };

    nixos-modules.url = "github:NuschtOS/nixos-modules";
  };
}
```

## Options

- `c3d2.addKnownHosts`: set to true to add the public ssh host keys to the known_hosts file.
- `c3d2.audioStreaming`: set to true to enable all services to audio stream to pulsebert, dacbert or glotzbert in hq.
  After enabling this option run ``systemctl --user restart pulseaudio`` or ``systemctl --user restart pipewire-pulse``.
  If that is not working logout of your session and log back in.
- `c3d2.addBinaryCache`: set to true to add the [c3d2 hydra](https://hydra.c3d2.de/) as permanent trusted-substituter.
- `c3d2.configurePrinting`: automatically configures printers located at c3d2
- `c3d2.configureWifi`: automatically configure the `C3D2` and `ZW public` (without auto connect) in the normal and legacy variants.

## Design

- Modules should never change the configuration without setting an option

## Structure

This is based on [sandro's nixos-modules (on GitHub)](https://github.com/superSandro2000/nixos-modules) and follows the same structure and design.
