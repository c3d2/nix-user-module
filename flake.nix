{
  description = "C3D2 NixOS user module";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-modules = {
      url = "github:NuschtOS/nixos-modules";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixos-modules, nixpkgs, self, ... }: {
    checks.x86_64-linux.load-modules = let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      inherit (pkgs) lib;
    in (nixpkgs.lib.nixosSystem {
      modules = [
        self.nixosModule
        # include a very basic config which contains fileSystem, etc. to avoid many assertions
        ({ modulesPath, ... }: {
          imports = lib.singleton "${modulesPath}/installer/cd-dvd/installation-cd-minimal.nix";
        })
      ];
      inherit system;
    }).config.system.build.toplevel;

    nixosModule = { config, lib, libS, ... }@args: {
      _module.args.libS = lib.mkOverride 1001 (nixos-modules.lib { inherit lib config; });

      imports = [
        nixos-modules.nixosModules.default
      ] ++ (dir: let
        ls = dir: lib.attrNames (builtins.readDir (./. + "/${dir}"));
      in map
        (file: ./. + "/${dir}/${file}")
        # assume that the nixos-modules repo is already imported if libS exists
        (lib.remove (lib.optionalString (lib.hasAttr "libS" args) "default.nix") (ls dir))
      ) "modules";
    };
  };
}
