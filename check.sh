#!/usr/bin/env bash
set -eou pipefail
set -x
nix flake check
nix flake check --override-input nixpkgs github:NixOS/nixpkgs/nixos-24.11
