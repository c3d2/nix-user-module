{ config, lib, ... }:

let
  cfg = config.c3d2;
in
{
  options = {
    c3d2.configureWifi = lib.mkEnableOption "configure the `C3D2` and `ZW Public` in the normall and legacy variants";
  };

  config = lib.mkIf cfg.configureWifi {
    networking.networkmanager.ensureProfiles = rec {
      profiles = {
        "C3D2" = {
          connection = {
            id = "C3D2";
            type = "wifi";
          };
          wifi = {
            mode = "infrastructure";
            ssid = "C3D2";
          };
          wifi-security = {
            key-mgmt = "sae";
            psk-flags = "1";
          };
          ipv4 = {
            method = "auto";
          };
          ipv6 = {
            addr-gen-mode = "default";
            method = "auto";
          };
        };
        "C3D2 legacy" = lib.recursiveUpdate profiles."C3D2" {
          connection.id = "C3D2 legacy";
          wifi.ssid = "C3D2 legacy";
        };
        "ZW public" = {
          connection = {
            autoconnect = false;
            id = "ZW public";
            type = "wifi";
          };
          wifi = {
            mode = "infrastructure";
            ssid = "ZW public";
          };
          ipv4 = {
            method = "auto";
          };
          ipv6 = {
            addr-gen-mode = "default";
            method = "auto";
          };
        };
        "ZW public legacy" = lib.recursiveUpdate profiles."ZW public" {
          connection.id = "ZW public legacy";
          wifi.ssid = "ZW public legacy";
        };
      };
    };
  };
}
