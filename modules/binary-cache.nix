{ config, lib, ... }:

let
  cfg = config.c3d2.addBinaryCache;
in
{
  options = {
    c3d2.addBinaryCache = lib.mkEnableOption "add hydra.hq.c3d2.de as a trusted substituter";
  };

  config = lib.mkIf cfg {
    nix.settings = {
      substituters = [ "https://hydra.hq.c3d2.de" ];
      trusted-public-keys = [ "hydra.hq.c3d2.de:KZRGGnwOYzys6pxgM8jlur36RmkJQ/y8y62e52fj1ps=" ];
    };
  };
}
