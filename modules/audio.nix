{ config, lib, options, pkgs, ... }:

let
  cfg = config.c3d2.audioStreaming;
  isGraphical = config.services.xserver.enable || config.services.displayManager.defaultSession != null;
  servicesPulseaudio = if (options.services or { })?pulseaudio then "services" else "hardware";
  cfgP = config.${servicesPulseaudio}.pulseaudio;
in
{
  options = {
    c3d2.audioStreaming = lib.mkEnableOption "enable all services requires to audio stream to dacbert, glotzert or pulsebert";
  };

  config = lib.mkIf cfg (lib.mkMerge [
    {
      # TODO: drop the mkMerge again when we drop compatibility for 24.11
      ${servicesPulseaudio}.pulseaudio = {
        # enabling zeroconf discovery or publishing turns on zeroconfSupport for the configured pulseaudio package
        # pulseaudioFull has that enabled plus it has a cache hit on cache.nixos.org
        package = pkgs.pulseaudioFull;
        zeroconf.discovery.enable = true;
      };
    }
    {
      environment.systemPackages = lib.mkIf isGraphical (with pkgs; [
        paprefs
        pavucontrol
        # https://wiki.archlinux.org/title/PulseAudio/Examples#PulseAudio_over_network
        (pkgs.writeScriptBin "enable-pipebert-rtp" /* bash */ ''
          set -eou pipefail
          export PATH=$PATH:${lib.makeBinPath [ cfgP.package dig ]}

          ip=$(dig +short pipebert.hq.c3d2.de)

          pactl load-module module-null-sink sink_name=rtp sink_properties="node.description='Pipebert\ RTP\ Stream'"
          pactl load-module module-rtp-send source=rtp.monitor destination_ip="$ip" port=9875
        '')
        (pkgs.writeScriptBin "disable-pipebert-rtp" /* bash */ ''
          set -eou pipefail
          export PATH=$PATH:${lib.makeBinPath [ cfgP.package ]}

          pactl unload-module module-rtp-send
          pactl unload-module module-null-sink
        '')
      ]);

      # required for paprefs to be able to save
      programs.dconf = lib.mkIf isGraphical {
        enable = true;
        profiles.user.databases = [{
          settings."org/freedesktop/pulseaudio/module-groups/zeroconf-discover" = {
            args0 = "";
            enabled = true;
            name0 = "module-zeroconf-discover";
          };
        }];
      };

      security.rtkit.enable = true;

      services = {
        avahi = {
          enable = true;
          nssmdns4 = true;
        };

        pipewire = {
          extraConfig.pipewire.zeroconf = {
            "context.modules" = [ {
              "name" = "libpipewire-module-zeroconf-discover";
            } ];
          };
          pulse.enable = true;
        };

        # makes avahi unreliable
        resolved.extraConfig = ''
          MulticastDNS=no
        '';
      };
    }
  ]);
}
