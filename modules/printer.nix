{ lib, config, pkgs, ... }:

let
  cfg = config.c3d2.configurePrinting;
in
{
  options = {
    c3d2.configurePrinting = lib.mkEnableOption "automatically configure printers located at c3d2";
  };

  config = lib.mkIf cfg {
    services.printing = {
      enable = true;
      drivers = [
        (pkgs.stdenv.mkDerivation {
          name = "xerox-workcenter-7132-ppd";
          src = pkgs.fetchzip {
            url = "https://download.support.xerox.com/pub/drivers/WC_7132/drivers/macosx/en/WorkCentre_7132_PS_genericPPD.zip";
            hash = "sha256-WfI6BGHipqqO8UG6N/yQ/QegCGb7JEvkOcqaB56E6To=";
            stripRoot = false;
          };
          installPhase = ''
            mkdir -p $out/share/cups/model/xerox
            cp 'Xerox WorkCentre 7132 PS' $out/share/cups/model/xerox
          '';
        })
      ];
    };

    hardware.printers.ensurePrinters = [{
      name = "Xerox_WorkCenter_7132";
      description = "Xerox WorkCentre 7132";
      location = "C3D2";
      deviceUri = "lpd://172.22.99.41/queue";
      model = "xerox/Xerox WorkCentre 7132 PS";
    }];
  };
}
