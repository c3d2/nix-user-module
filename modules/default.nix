{ lib, ... }:

{
  # only included here to prevent errors about using a undefined option
  options.opinionatedDefaults = lib.mkEnableOption "opinionated defaults";
}
